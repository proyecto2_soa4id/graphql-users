<?php

use Zend\Config\Factory;
use Firebase\JWT\JWT;
require_once __DIR__ . '/authentication.php';

return [
    
    # Crea un usuario
    'createUser' => function($root, $args, $context) {
        if (!array_key_exists('username', $args)){
            throw new UserException('Username empty...');
        }
        if (!array_key_exists('password', $args)){
            throw new UserException('Password empty...');
        }
        
        $username = $args['username'];
        $password = $args['password'];
        $config = Factory::fromFile('config/config.php', true);
        
        $dsn = 'pgsql:host=' . $config->get('database')->get('host') . ';dbname=' . $config->get('database')->get('name') . ';port=' . $config->get('database')->get('port');
        $db = new PDO($dsn, $config->get('database')->get('user'), $config->get('database')->get('password'));
        
        $sql = 'SELECT id FROM users WHERE  username = ?';
        $stmt = $db->prepare($sql);
        $stmt->execute([$username]);
        $rs = $stmt->fetch();
        
        if($rs) {
            throw new UserException("User already exists...");
        }
        
        try {
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = 'INSERT INTO users (username, password) VALUES (?, ?)';
            $stmt = $db->prepare($sql);
            $stmt->execute([$username, $password]);
            return "User created successfully...";
        } 
        catch(PDOException $e) {
            throw new DBException( "Error: " . $sql . $e->getMessage());
        }
        $db->close();
    },

    # Actualiza el username de un usuario
    'updateUser' => function($root, $args, $context) {

        if (!array_key_exists('username', $args)) {
            throw new UserException('Username empty...');
        }
        
        $username = $args['username'];
        $new_username = $args['new_username'];
        $config = Factory::fromFile('config/config.php', true);
        
        $dsn = 'pgsql:host=' . $config->get('database')->get('host') . ';dbname=' . $config->get('database')->get('name') . ';port=' . $config->get('database')->get('port');
        $db = new PDO($dsn, $config->get('database')->get('user'), $config->get('database')->get('password'));
        
        $sql = 'SELECT id FROM users WHERE  username = ?';
        $stmt = $db->prepare($sql);
        $stmt->execute([$new_username]);
        $rs = $stmt->fetch();
        
        if($rs) {
            throw new UserException("User already exists...");
        }
        
        try {
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = 'UPDATE users SET username = ? WHERE username = ?';  
            $stmt = $db->prepare($sql);
            $stmt->execute([$new_username, $username]);
            return "User updated successfully...";
        } 
        catch(PDOException $e) {
            throw new DBException( "Error: " . $sql . $e->getMessage());
        }
        $db->close();
    },

    # Se elimina un usuario
    'deleteUser' => function($root, $args, $context) {

        if (!array_key_exists('username', $args)) {
            throw new UserException('Username empty...');
        }
        
        $username = $args['username'];
        $config = Factory::fromFile('config/config.php', true);
        
        $dsn = 'pgsql:host=' . $config->get('database')->get('host') . ';dbname=' . $config->get('database')->get('name') . ';port=' . $config->get('database')->get('port');
        $db = new PDO($dsn, $config->get('database')->get('user'), $config->get('database')->get('password'));
        
        $sql = 'SELECT id FROM users WHERE  username = ?';
        $stmt = $db->prepare($sql);
        $stmt->execute([$username]);
        $rs = $stmt->fetch();
        
        if($rs) {
            try {
                $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $sql = 'DELETE FROM users WHERE username = ?';  
                $stmt = $db->prepare($sql);
                $stmt->execute([$username]);
                return "User deleted successfully...";
            } 
            catch(PDOException $e) {
                throw new DBException( "Error: " . $sql . $e->getMessage());
            }
        }

        $db->close();
    },

    # Se obtiene el usuario a partir del token 
    'getUser' => function($root, $args, $context) {
        if (!array_key_exists('token', $args)) {
            throw new UserException('Field token is empty...');
        }

        $jwt = (array) decodeJWT($args['token']);
        $config = Factory::fromFile('config/config.php', true);

        $dsn = 'pgsql:host=' . $config->get('database')->get('host') . ';dbname=' . $config->get('database')->get('name') . ';port=' . $config->get('database')->get('port');
        $db = new PDO($dsn, $config->get('database')->get('user'), $config->get('database')->get('password'));

        $sql = 'SELECT id, username FROM   users WHERE  id = ?';
        $stmt = $db->prepare($sql);
        $data = (array) $jwt["data"];
        $id = $data["userId"];
        $stmt->execute([$id]);
        $rs = $stmt->fetch();
        if ($rs) {
            return $rs['username'];
        }
        else {
            throw new UserException("Unknown User Id");
        }
    },

    # Se crea el token de autenticacion
    'createToken' => function($root, $args, $context) {
        if (!array_key_exists('username', $args)) { 
            throw new UserException('Username empty...');
        }
        if (!array_key_exists('password', $args)) {
            throw new UserException('Password empty...');
        }
        $username = $args['username'];
        $password = $args['password'];
        $config = Factory::fromFile('config/config.php', true);

        $db_conf = 'pgsql:host=' . $config->get('database')->get('host') . ';dbname=' . $config->get('database')->get('name') . ';port=' . $config->get('database')->get('port');
        $db = new PDO($db_conf, $config->get('database')->get('user'), $config->get('database')->get('password'));

        $sql = <<<EOL
        SELECT id, username, password FROM users WHERE username = ?
EOL;
        
        $stmt = $db->prepare($sql);
        $stmt->execute([$username]);
        $rs = $stmt->fetch();

        $not_before = 5;
        $expiration_time = 60000;

        if ($rs) {
            if ($password == $rs['password']) {
                
                $tokenId    = base64_encode(openssl_random_pseudo_bytes(32));
                $issuedAt   = time();
                $notBefore  = $issuedAt + $not_before;  
                $expire     = $notBefore + $exp_time;
                $serverName = $config->get('serverName');
                
                $data = [
                    'iat'  => $issuedAt,
                    'jti'  => $tokenId, 
                    'iss'  => $serverName,       
                    'nbf'  => $notBefore,       
                    'exp'  => $expire,         
                    'data' => [                 
                        'userId'   => $rs['id'], 
                        'userName' => $username, 
                    ]
                ];
                
                $secret_key = base64_decode($config->get('jwt')->get('key'));
                $algorithm = $config->get('jwt')->get('algorithm');            
                    
                $jwt = JWT::encode(
                    $data,      
                    $secret_key, 
                    $algorithm  
                    );                
                    
                return $jwt;

            } 
            else {
                throw new UserException('Password doesn\'t match');
            }
        } 
        else {
            throw new UserException('User doesn\'t exist');
        }
    },    
    
    'prefix' => ''
];