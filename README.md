## Instalation

In order to run PHP server, you must run those commands first:

`
sudo apt install php7.2-cli composer
`

`
sudo apt-get install php7.2-pgsql
`

`
sudo apt-get install php7.2-mbstring
`

`
composer install
`

## Execution

Then, in you want to execute the server, execute this command:

`
php -S <host>:<port> ./main.php
`
