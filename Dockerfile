FROM dmiseev/php-fpm7.2
ADD . /app
WORKDIR /app
RUN composer install
EXPOSE 5002
CMD ["php", "-S" , "0.0.0.0:5002",  "./main.php"]
