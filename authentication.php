<?php

use GraphQL\Error\ClientAware;
use Zend\Config\Factory;
use Firebase\JWT\JWT;

class DBException extends \Exception implements ClientAware {

    public function isClientSafe() {
        return true;
    }

    public function getCategory() {
        return 'DBError';
    }
}

class UserException extends \Exception implements ClientAware {

    public function isClientSafe() {
        return true;
    }

    public function getCategory() {
        return 'UserError';
    }
}

function decodeJWT($jwt) {

    $config = Factory::fromFile('config/config.php', true);
    $secret_key = base64_decode($config->get('jwt')->get('key'));
    $algorithm = $config->get('jwt')->get('algorithm');

    try {
        $token_decoded = JWT::decode($jwt, $secret_key, array($algorithm));
    }

    catch (Firebase\JWT\ExpiredException $err) {
        throw new UserException("Token Expired");
    }

    catch(Firebase\JWT\SignatureInvalidException $err) {
        throw new UserException("Signature Invalid");
    }

    catch(Firebase\JWT\BeforeValidException $err) {
        throw new UserException("Before Valid");
    }
    return $token_decoded;
}

?>